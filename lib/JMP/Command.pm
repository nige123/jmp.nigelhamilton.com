package JMP::Command;

#------------------------------------------------------------------------------------------------
#
# Command.pm 
#
# Description:  Superclass for commands
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;
use JMP::Template;


has 'template' => sub {
	JMP::Template->new
};


sub help {
    die "Please include a more detailed help message in your command";
}


sub name {
    die "Please add a unique name attribute for your command";
}


sub run {
    die "Please include a run method in your command";
}


sub usage {
    die "Please include a one line usage attribute in your command";
}

sub execute {

	my ($self, $command_template, $arguments) = @_;

	my $command = $self->template->render($command_template, $arguments);

	# execute the command 	
	system($command); 

}


1;
