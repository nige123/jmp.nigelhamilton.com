package JMP::Command::Check;

#------------------------------------------------------------------------------------------------
#
# Check.pm 
#
# Description:  Check a Perl program for syntax and errors and jmp to fix them
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;

has 'help'  => <<HELP;

jmp check <perl5-filename>

HELP

has 'name'  => 'check';
has 'usage' => 'check the syntax of a Perl script and jmp to any errors';


sub run {

    my ($self, $perl_filename) = @_;

    say "Usage: $0 <perl_filename>"      unless $perl_filename;   
    say "File not found: $perl_filename" unless -e $perl_filename;

    # execute perl - capture errors and output
    # TODO - what is the path to Perl on the local system?
    my ($stdout, $stderr) = $self->_run_command('/usr/bin/perl -c', $perl_filename);

    my $output = $stdout // 'No Output';
    my $errors = $stderr // 'None';
    
    my @action_templates = $self->_get_action_templates($output, $errors);

    # show page 1 of the results
    JMP::Screen->new(
    	title 			           =>  "jmp check $perl_filename",
    	action_templates           =>  \@action_templates,
        action_templates_per_page  =>  20,
        global_actions => [
        	{ text => 'back', 		action => "$0 back" },
        	{ text => 'check', 		action => "$0 check $perl_filename" },        	
        	{ text => 'tidy', 		action => "$0 tidy $perl_filename" },        	
        	{ text => 'edit', 		action => "$0 edit $perl_filename" },        	
        	{ text => 'reload', 	action => "$0 check $perl_filename" },        	
        	{ text => 'view', 	    action => "$0 view $perl_filename" },        	
        ]
    )->display_page(1);

}


sub _get_action_templates {

    my ($self, $stdout, $stderr) = @_;

    my @action_templates;

    my @stdout_lines = split(/\n/, $stdout);

    foreach my $stdout_line (@stdout_lines) {
        push(@action_templates, 
                JMP::Screen::ActionTemplate->new(
                    template => $stdout_line . "\n"
                )
            );
    }

    # grab stderr as lines
    my @error_lines = split(/\n/, $stderr);

    # read in the error lines
    foreach my $line (@error_lines) {

        # parse the error output looking for line numbers
        if ($line =~ /at (\S+) line (\d+)/) {

            my $error_file  = $1;
            my $line_number = $2;
            
            $line   =~ s/at $error_file/at \[\[\-uppercase_key\-\]\] $error_file/;
            $line   =~ s/line $line_number/line \[\[\-lowercase_key\-\]\] $line_number/;

            push(@action_templates, 
                    JMP::Screen::ActionTemplate->new(
                        template              => $line . "\n",
                        lowercase_key_command => "$0 edit $error_file $line_number",
                        uppercase_key_command => "$0 view $error_file"
                    )
            );

        }
        else {

            push(@action_templates, 
                    JMP::Screen::ActionTemplate->new(
                        template => $line . "\n"
                    )
            );

        }

     }

    return @action_templates;
    
}


###############################################################################
#
# _run_command - execute a command and return the results
#
###############################################################################

sub _run_command {
    
    my ($self, $command, $full_path) = @_;

    # execute perl - capture errors and output
    # TODO - cross-platform /tmp files would be good too
    my $stdout_file = "/tmp/$$-check-stdout.jmp";
    my $stderr_file = "/tmp/$$-check-stderr.jmp";
    
    # execute the perl command
    system("$command $full_path 1>$stdout_file 2>$stderr_file");

    # pull in stdout
    my $stdout = File::Slurp::read_file($stdout_file);
    my $stderr = File::Slurp::read_file($stderr_file);
    
    #$self->log->notice("stdout = $stdout and stderr = $stderr");
    #$self->log->notice("contents = $stderr ");

    # remove the /tmp file
    unlink($stdout_file);
    unlink($stderr_file);

    return ($stdout, $stderr);
    
}


1;
