package JMP::Command::DB;

#------------------------------------------------------------------------------------------------
#
# DB.pm 
#
# Description:  Jump to a database.
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;

has 'help'  => <<HELP;

jmp db

HELP

has 'name'  			=> 'db';
has 'usage' 			=> 'jmp to your database';


sub run {

    my ($self) = @_;
	
	# grab the database details from config file
	my $db_command = $self->config->get->{commands}->{db}->{shell_command};
	
	return say "Add a command to launch your database in your jmp.conf file (commands -> db -> shell_command): " . $self->config->config_location 
		unless $db_command;
	
	# call the db command
	system($db_command);	

}

1;
