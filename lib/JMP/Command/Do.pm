package JMP::Command::Do;

#------------------------------------------------------------------------------------------------
#
# Do.pm 
#
# Description:  Edit a simple 123.do file.
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use File::HomeDir;
use JMP::Command -base;
use JMP::Command::Edit;
use JMP::Util::Git;
use Path::Tiny;

has git_root => sub { JMP::Util::Git->new->git_root };

has 'help'  => <<HELP;

jmp do

HELP

has 'name'  => 'do';
has 'usage' => 'update your 123.do file: do, doing, done';


sub run {

    my ($self) = @_;

	my $do_location = ($self->git_root)
					? $self->git_root . '/123.do'
					: File::HomeDir->my_home . '/123.do'; 
		
	$self->_create_do_file($do_location) 
		unless -e $do_location;
	
	# IDEA: check for config errors here too after editing!
	JMP::Command::Edit->new->run($do_location);
	
}


sub _create_do_file {

	my ($self, $do_location) = @_;
	
	# the 123.do file format makes it easy to move things from 1. do -> 2. doing -> 3. done
	my $do_123 = <<DO;
	
1. DO
====================================================================================================

# add the next things todo in the Do section
NH - remember to pick up some milk


2. DOING (max 2 per person)
====================================================================================================

# when you start actually doing a task move it into the Doing section - no more than two at a time!
NH - explain the doing section


3. DONE
====================================================================================================

# once a task is complete move into Done. Add the date if it is a new day.

12/03/2014
----------
NH - created version 1 of this file - based on http://123.do
NH - once something is done move it into a section underneath today's date
		
DO

	path($do_location)->spew($do_123);
	
}

1;
