package JMP::Command::File;

#------------------------------------------------------------------------------------------------
#
# File.pm 
#
# Description:  locate powered file search
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;

has 'help'  => <<HELP;

jmp file <filename>

HELP

has 'name'  		=> 'file';
has 'usage' 		=> 'locate a file in the filesystem and jump to it';


sub run {

    my ($self, $filename) = @_;

    say "Usage: $0 <filename>" unless $filename;      

    # search using git grep for matching lines
    my @matching_files   = $self->_get_matching_files($filename);

    # create action templates
    my @action_templates = $self->_get_action_templates(@matching_files);

    # show page 1 of the results
    JMP::Screen->new(
    	title 			          =>  "jmp file $filename",
        action_templates_height   => 1,
    	action_templates          =>  \@action_templates,
    	global_actions			  => [
    		{ text => 'back', action => "$0 back" }
    	]
    )->display_page(1);

}


sub _get_action_templates {

    my ($self, @filenames) = @_;

    my @action_templates;

    foreach my $filename (@filenames) {

        my $template = "[[-lowercase_key-]] [[-uppercase_key-]] $filename";

        my $action = JMP::Screen::ActionTemplate->new(
                        template                =>  $template, 
                        uppercase_key_command   =>  "$0 view $filename",
                        lowercase_key_command   =>  "$0 edit $filename 0"
                    );

        push(@action_templates, $action);

    }

    return @action_templates;

}


sub _get_matching_files {

    my ($self, $filename) = @_;

	# use the updatedb to find matching files fast
	# -e matches only existing files
    my @matching_files  = `locate -e $filename`;

    return @matching_files;

}


1;
