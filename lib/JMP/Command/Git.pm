package JMP::Command::Git;

#------------------------------------------------------------------------------------------------
#
# Git.pm
#
# Description:  Jmp to git-related files
# Author: 		Nigel Hamilton (nige@123.do)
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use Cwd;
use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;
use JMP::Util::Git;

has 'git_root' => sub { JMP::Util::Git->new->git_root };

# NEXT: add file globs
has 'help' => <<HELP;

jmp git <status>

HELP

has 'name'  => 'git';
has 'usage' => 'jump to files shown in git status';

sub run {

    my ($self) = @_;

    # create action templates
    my @action_templates = $self->_get_action_templates;

    # show page 1 of the results
    JMP::Screen->new(
        title                   => "jmp git status",
        action_templates_height => 1,
        action_templates        => \@action_templates,
        global_actions          => [ { text => 'back', action => "$0 back" } ]
    )->display_page(1);

}

sub _get_action_templates {

    my ($self) = @_;

    my @git_status_lines = `git status --porcelain`;

    return unless @git_status_lines;

    my @action_templates;

    foreach my $git_status_line (@git_status_lines) {

        my $action;

        # match the porcelain output of git status
        if ( $git_status_line =~ m{(..)\s(.*?)$} ) {

            my $status   = $1;
            my $filename = $2;

            my $full_path = $self->git_root . '/' . $filename;

            $action = JMP::Screen::ActionTemplate->new(
                template =>
                    " [[-lowercase_key-]] [[-uppercase_key-]]  $status  $filename\n",
                lowercase_key_command => "$0 edit $full_path",
                uppercase_key_command => "$0 view $full_path"
            );

        }

        push( @action_templates, $action ) if $action;

    }

    return @action_templates;

}

1;
