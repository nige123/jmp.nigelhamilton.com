package JMP::Command::Google;

#------------------------------------------------------------------------------------------------
#
# Google.pm 
#
# Description:  Perform a Google search
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Template;
use JMP::Command -base;

has 'help'  => <<HELP;

jmp google <search-terms>

HELP

has 'name'  => 'google';
has 'usage' => 'search Google';


sub run {

    my ($self, @keywords) = @_;

    my $search_terms = join(' ', @keywords);

    return say $self->help unless $search_terms;      

	my $url_template = 	$self->config->{commands}->{google}->{url_template} 
					 || 'https://www.google.co.uk/#q=[-search_terms-]';  


    my $search_url = JMP::Template->new->render_url(
    					$url_template,
						{ search_terms => $search_terms }
    				 );

	# jmp to the search url
	$self->execute("$0 url '[-url-]'", { url => $search_url });

}

1;
