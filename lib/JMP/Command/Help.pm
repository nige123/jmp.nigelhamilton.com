package JMP::Command::Help;

#------------------------------------------------------------------------------------------------
#
# Help.pm
# Description:  Display help for command(s)
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;

has commands => sub { {} };    # a list of all the available commands
has usage => 'display command help';
has help  => <<HELP;

jmp help [command]

HELP

has name => 'help';

sub run {

    my ( $self, $command ) = @_;

    return say $self->show_all_commands unless $command;
    return say $self->help if $command eq 'help';
    return say $self->commands->{$command}->help;

}

sub show_all_commands {

    my ($self) = @_;

    say '';

    foreach my $command ( sort ( 'help', keys %{ $self->commands } ) ) {

        if ( $command eq 'help' ) {
            say '  ' . $command . "\t\t- " . $self->usage;
        }
        else {
            say '  ' . $command . "\t\t- "
                . $self->commands->{$command}->usage;
        }

    }

    say '';

}

1;
