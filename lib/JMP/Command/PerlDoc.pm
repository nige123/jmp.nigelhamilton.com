package JMP::Command::PerlDoc;

#------------------------------------------------------------------------------------------------
#
# PerlDoc.pm 
#
# Description:  Display the perldoc in a module or file
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;
use Module::Util;
use Path::Tiny;

has 'help'  => <<HELP;

jmp perldoc <filename|module>

HELP

has 'name'  => 'perldoc';
has 'usage' => 'display the perldoc for a file';


sub run {

    my ($self, $filename_or_module) = @_;

    return say $self->help unless $filename_or_module;   

    system("perldoc $filename_or_module");

}


1;
