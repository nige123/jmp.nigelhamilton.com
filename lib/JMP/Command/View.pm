package JMP::Command::View;

#------------------------------------------------------------------------------------------------
#
# View.pm
#
# Description:  Show a view of a file so you can easily navigate around it
#
# Author: 		Nigel Hamilton (nige@123.do)
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;
use Module::Util;
use Path::Tiny;

has 'help' => <<HELP;

jmp view <filename>

HELP

has 'name'  => 'view';
has 'usage' => 'view the contents of a file';

sub run {

    my ( $self, $filename ) = @_;

    say $self->help unless $filename;
    say "File not found: $filename" unless -e $filename;

    my @view_lines = path($filename)->lines;

    my @action_templates
        = $self->_get_action_templates( $filename, @view_lines );

    # show page 1 of the results
    JMP::Screen->new(
        title                     => "jmp view $filename",
        action_templates          => \@action_templates,
        action_templates_per_page => 20,
        global_actions            => [
            { text => 'exit',    action => "$0 exit" 			  },
            { text => 'check',   action => "$0 check   $filename" },
            { text => 'tidy',    action => "$0 tidy    $filename" },
            { text => 'edit',    action => "$0 edit    $filename" },
            { text => 'reload',  action => "$0 view    $filename" },
            { text => 'perldoc', action => "$0 perldoc $filename" },
        ],
    )->display_page(1);

}

sub _get_action_template {

    my ( $self, $filename, $view_line, $line_number ) = @_;

    if ( $view_line =~ m/^\s*use\s+(\S+)/ ) {

        my $module = $1;
        $module =~ s/\;//g;
        my $module_fs_path = Module::Util::find_installed($module) // '';

        return JMP::Screen::ActionTemplate->new( template => $view_line )
            unless -e $module_fs_path;

        return JMP::Screen::ActionTemplate->new(
            lowercase_key_command => "$0 edit $filename $line_number",
            uppercase_key_command => "$0 view $module_fs_path",
            template =>
                "[[-lowercase_key-]] ($line_number) uses [[-uppercase_key-]] $module\n",
        );
    }

    if ( $view_line =~ m/^\s*has\s+(\S+)/ ) {

        my $has_attribute = $1;
        $has_attribute =~ s/'//g;

        return JMP::Screen::ActionTemplate->new(
            lowercase_key_command => "$0 edit $filename $line_number",
            uppercase_key_command => "$0 find $has_attribute",
            template =>
                "[[-lowercase_key-]] ($line_number) has [[-uppercase_key-]] $has_attribute\n",
        );

    }

    if ( $view_line =~ m/^\s*sub\s+(\w+)/ ) {
        my $subroutine = $1;

        return JMP::Screen::ActionTemplate->new(
            lowercase_key_command => "$0 edit $filename $line_number",
            uppercase_key_command => "$0 find $subroutine",
            template =>
                "[[-lowercase_key-]] ($line_number) sub/method [[-uppercase_key-]] $subroutine\n",
        );

    }

	# TODO - get this from "find" config later
	my @file_types = ('\.tpl', '\.page', '\.widget');

	# does the line contain a findable file type?
	foreach my $file_type (@file_types) {
		
        if ($view_line =~ m/(\w+$file_type)/) {

			my $potential_filename = $1;

			return JMP::Screen::ActionTemplate->new(
    	        lowercase_key_command => "$0 edit $filename $line_number",
        	    uppercase_key_command => "$0 file $potential_filename",
            	template =>
                	"[[-lowercase_key-]] ($line_number) refers to [[-uppercase_key-]] $potential_filename \n",
			);

		}		

	}

}


sub _get_action_templates {

    my ( $self, $filename, @view_lines ) = @_;

    my @actions;

    my $line_number = 0;

    foreach my $view_line (@view_lines) {

        $line_number++;

        my $action = $self->_get_action_template( $filename, $view_line,
            $line_number );

        push( @actions, $action ) if $action;

    }

    return @actions;

}

1;
