package JMP::Object;

#------------------------------------------------------------------------------------------------
#
# Object.pm 
#
# Description:  Super object
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use Mojo::Base -base;

has config => sub {

	require JMP::Config;
	
	my $config = JMP::Config->new;
	
	$config->load;
	
	# return a config hash
	return $config;

};


sub dump {
    
    my ($self, $thing) = @_;

    require Data::Dumper;
    
    say Dumper($thing);

}

1;
