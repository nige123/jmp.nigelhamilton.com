package JMP::Screen::ActionTemplate;

#------------------------------------------------------------------------------------------------
#
# ActionTemplate.pm 
#
# Description:  Display an action line may contain a lowercase and uppercase letter
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;
use JMP::Template;
use Term::ANSIColor qw(:constants);

has lowercase_key			=>	'';
has lowercase_key_command 	=> 	'';

has template 				=> 	''; 
has template_engine 		=>  sub { JMP::Template->new };

has uppercase_key			=>	'';
has uppercase_key_command 	=> 	'';

sub render {

	my ($self) = @_;
		
	my $tokens = {
		uppercase_key => $self->uppercase_key,
		lowercase_key => $self->lowercase_key,
	};
	
	my $rendered_template = $self->template_engine->render($self->template, $tokens);
	
	return $self->_highlight_keys($rendered_template);
	
}


sub _highlight_keys {

	my ($self, $rendered_template) = @_;

	# highlight options
	return $rendered_template unless $rendered_template =~ /\[/;

	my $title     = BLACK ON_GREEN;
	my $highlight = WHITE;            # select options in questions
	my $lowlight  = BLUE;             # informative options
	my $neonlight = GREEN;            # BOLD!
	my $reset     = RESET;            # needed for interpolation

	# highlight any option keys
	$rendered_template =~ s{\[([a-zA-Z])\]}{\[$highlight$1$reset\]}g;	

	return $rendered_template;
	
}

1;

