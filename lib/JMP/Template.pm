#!/usr/bin/perl

package JMP::Template;

use JMP::Object -base;
use Template::Simple;		# fast, simple no code in the template - templates!


# tokens look like [-this-]
has pre_token_delimiter  => sub { qr/\[\-/ };
has post_token_delimiter => sub { qr/\-\]/ };


has 'template_simple' => sub {
	my ($self) = @_;
	Template::Simple->new(
		pre_delim 	=> 	$self->pre_token_delimiter,
		post_delim	=>	$self->post_token_delimiter,
	
	);
};


sub render {

    my ($self, $string, $tokens) = @_;

	# return the string - not just a reference
	return ${ $self->template_simple->render($string, $tokens) };
	
}

sub render_url {

    my ($self, $string, $tokens) = @_;

	require URI::Escape;
	
	while (my ($key, $value) = each %$tokens) {
		$tokens->{$key} = URI::Escape::uri_escape($value);
	}

	return $self->render($string, $tokens);
	
}

1;






















