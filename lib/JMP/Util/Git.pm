package JMP::Util::Git;

#------------------------------------------------------------------------------------------------
#
# Git.pm 
#
# Description:  Help with managing git
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;

# find the git root directory
has git_root => sub {

	my $git_root = `git rev-parse --show-toplevel`;
	chomp($git_root);
	return $git_root;

};


1;
